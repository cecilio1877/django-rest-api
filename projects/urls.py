from rest_framework import routers
from .api import ProjectViewSet

router = routers.DefaultRouter() # crea las rutas por defecto automaticamente

router.register('api/projects', ProjectViewSet, 'projects')

urlpatterns = router.urls